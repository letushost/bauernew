<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;

class PageController extends Controller {
	public function home()
	{
		return view('pages.home');
	}

	public function services($type = false)
	{
		return view('pages.services', ['services' => Service::orderBy('display_order', 'ASC')->get(), 'type' => $type]);
	}

	public function jointheteam()
	{
		return view('pages.jointheteam', ['services' => Service::orderBy('display_order', 'ASC')->get()]);
	}

	public function meettheteam()
	{
		return view('pages.meettheteam');
	}

	public function contact()
	{
		return view('pages.contact');
	}

	public function cctv()
	{
		return view('pages.cctv');
	}
}