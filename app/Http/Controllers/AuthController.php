<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {
	public function standard(Request $request)
    {
        if(Auth::attempt($request->only('email', 'password'))) {
        	$user = User::find(auth()->id());
        	$user->api_token = str_random(40);
        	$user->save();

        	return $user;
        }

        return app()->abort(401);
    }
}