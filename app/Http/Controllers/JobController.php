<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobController extends Controller {
	public function my()
	{
		$jobs = Job::where('assigned_to', auth()->id())->whereDate('starts_at', Carbon::now())->get();
		return $jobs;	
	}

	public function single(Job $job)
	{
		return $job;
	}

	public function checkin(Job $job, Request $request)
	{
		// Check we've sent long/lat.
		if(!$request->has('longitude') || !$request->has('latitude'))
			return app()->abort(422);

		$distance = Job::calculateDistance($request->latitude, $request->longitude, $job->job_latitude, $job->job_longitude);

		if($distance <= 0.02) { // 0.01 miles = 16.09344 metres.
			$job->checkin_longitude = $request->longitude;
			$job->checkin_latitude = $request->latitude;
			$job->checkin_at = Carbon::now();
			$job->save();
			return $job;
		}

		return app()->abort(403);
	}
}