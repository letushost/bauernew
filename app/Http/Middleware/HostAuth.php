<?php

namespace App\Http\Middleware;

use Closure;
use \App\User;
use Illuminate\Support\Facades\Auth;

class HostAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check we've been sent a token.
        if(!$request->bearerToken())
            return app()->abort(401); // 401 unauthorized

        // Find the token we've been sent.
        $user = User::where('api_token', $request->bearerToken())->first();

        // The sent token was invalid.
        if(!$user)
            return app()->abort(401, 'unauthorized'); // 401 unauthorized

        // Set the auth user and go to next.
        Auth::setUser($user);
        return $next($request);
    }
}
