<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bauer Security</title>
        
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        @include('common.slidemenu')

        <div id="content">
            @yield('content')
            @include('common.fixedfooter')
            @include('common.footer')
        </div>

        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/slideout.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var breakpoint = ($('.page-home').length > 0) ? 925 : 170;

                // Set now?
                if($(window).scrollTop() > breakpoint) {
                    $('.fixed-footer').fadeIn(500);
                }

                // Fixed footer
                $(window).scroll(function() {
                    if($(window).scrollTop() > breakpoint) {
                        return $('.fixed-footer').fadeIn(500);
                    }

                    return $('.fixed-footer').fadeOut(500);
                });

                // Slideout
                window._slideout = new Slideout({
                    'panel': document.getElementById('content'),
                    'menu': document.getElementById('slide-menu'),
                    'padding': 280,
                    'tolerance': 70,
                    'touch': true,
                });

                $('.open-menu').on('click', function() {
                    // $('#slide-menu').show();
                    window._slideout.toggle();
                });
            });
        </script>
        @yield('scripts')
	</body>
</html>