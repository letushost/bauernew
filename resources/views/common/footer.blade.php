<footer>
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="item">
						<h2>Address</h2>
						<p>Unit 10, Bulrushes Farm Business Park<br />Coombe Hill Road<br />East Grinstead<br />RH19 4LZ</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="item">
						<h2>Social</h2>
						<ul class="social">
							@include('common.sociallinks')
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="item">
						<h2>Contact</h2>
						<p>0845 241 0074<br />ops@bauersecurity.co.uk<br />admin@bauersecurity.co.uk<br />support@bauersecurity.co.uk</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<img src="/img/logo-text.png" />
	<p>&copy; Bauer Security 2014 - 2018</p>
</footer>