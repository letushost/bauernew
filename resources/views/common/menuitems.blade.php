<li><a href="/" @if(Route::currentRouteName() == 'home')class="active"@endif>Home</a></li>
<li><a href="/services" @if(Route::currentRouteName() == 'services')class="active"@endif>Services</a></li>
<li><a href="/cctv" @if(Route::currentRouteName() == 'cctv')class="active"@endif>CCTV</a></li>
v<li><a href="/meet-the-team" @if(Route::currentRouteName() == 'meet-the-team')class="active"@endif>The Team</a></li>
<li><a href="/join-the-team" @if(Route::currentRouteName() == 'join-the-team')class="active"@endif>Jobs</a></li>
<li><a href="/contact" @if(Route::currentRouteName() == 'contact')class="active"@endif>Contact Us</a></li>