<header class="condensed">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<a href="/">
					<img src="/img/logo-condensed-extra.png" />
				</a>
			</div>
			<div class="col-md-10">
				<ul>
					@include('common.menuitems')
				</ul>
			</div>
		</div>
	</div>
</header>
<div class="post-header">
	<div class="container clearfix">
		<ul class="info">
			<li>ops@bauersecurity.co.uk</li>
			<li>0845 241 0074</li>
		</ul>

		<ul class="social">
			@include('common.sociallinks')
		</ul>
	</div>
</div>