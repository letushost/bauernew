@extends('layouts.base')
@include('common.header')

@section('content')
	<div class="page page-services-masonry">
		<div class="page-title">
			<div class="container">
				<h2>Our Services</h2>
				<p>Learn more about what <strong>Bauer Security</strong> can do for you.</p>
			</div>
		</div>

		<section class="services">
			<div class="container">
				@foreach($services as $service)
					<div class="service clearfix">
						<img src="/img/services/{{ $service->image }}" class="img-fluid" />
						<div class="info">
							<h2>{{ $service->name }}</h2>
							<p>{{ $service->description }}</p>
						</div>
					</div>
				@endforeach
			</div>
		</section>
	</div>
@endsection