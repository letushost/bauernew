@extends('layouts.base')

@section('content')
	@include('common.header')
	<section class="page page-services">
		<div class="page-title">
			<div class="container">
				<h2>Our Services</h2>
				<p>Learn more about what <strong>Bauer Security</strong> can do for you.</p>
			</div>
		</div>

		<div class="services">
			<div class="container">
				<ul>
					<li><a href="#" @if(!$type)class="active"@endif data-category="all">All</a></li>
					<li><a href="#" @if($type == 'construction')class="active"@endif data-category="construction">Construction</a></li>
					<li><a href="#" @if($type == 'entertainment')class="active"@endif data-category="entertainment">Entertainment</a></li>
					<li><a href="#" @if($type == 'education')class="active"@endif data-category="education">Education</a></li>
					<li><a href="#" @if($type == 'corporate')class="active"@endif data-category="corporate">Corporate</a></li>
				</ul>

				<div class="service-list">
					<div class="container">
						<div class="row">
							@foreach($services as $i => $service)
								<!-- <div class="col-md-6">
									<div class="service all {{ $service->class }}">
										<img src="/img/services/{{ $service->image }}" class="img-fluid" />
										<div class="content">
											<h2>{{ $service->name }}</h2>
											<h3>Education, Entertainment</h3>
											<p>{{ $service->description }}</p>
										</div>
									</div>
								</div> -->
								<div class="service clearfix all {{ $service->class }}">
									<img src="/img/services/{{ $service->image }}" class="img-fluid" />
									<div class="info">
										<h2>{{ $service->name }}</h2>
										<p>{{ $service->description }}</p>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {

			var category = $('.services ul li a.active').data('category');
			$('.service').not('.' + category).hide();
			$('.service.' + category).show();

			$('.services ul li a').on('click', function(e) {
				e.preventDefault();

				$('.services ul li a.active').removeClass('active');
				$(this).addClass('active');

				var category = $(this).data('category');
				$('.service').not('.' + category).hide();
				$('.service.' + category).show();

				history.pushState({
				    id: 'services'
				}, null, '/services/' + category);
			});
		});
	</script>
@endsection