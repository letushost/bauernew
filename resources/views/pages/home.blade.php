@extends('layouts.base')

@section('content')
	<div class="page page-home">
		<div class="hero hero-home">
			<div class="slider">
				<div class="slide active slide-1"></div>
				<div class="slide slide-2"></div>
				<div class="slide slide-3"></div>
			</div>

			<div class="container">
				<ul class="social animated slideInDown">
					@include('common.sociallinks')
				</ul>

				<ul class="details animated slideInDown">
					<li><a href="mailto:ops@bauersecurity.co.uk" style="color: #fff;">ops@bauersecurity.co.uk</a></li>
					<li><a href="#" style="color: #fff;">0845 241 0074</a></li>
				</ul>

				<div class="logo animated slideInDown">
					<a href="/">
						<img src="/img/logo-clear.png" class="img-fluid" />
					</a>
				</div>

				<ul class="main-nav animated bounceIn">
					@include('common.menuitems')
				</ul>

				<div class="caption animated flipInX">
					<h2>15 years experience</h2>
					<h3>Working in the security industry</h3>
					<hr />
					<p>Bauer Security - Established 2009</p>
					<span class="fa fa-arrow-circle-down"></span>
				</div>
			</div>
		</div>

		<section class="story">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2>The Story</h2>
						<h3>How it all began</h3>
						<p>Bauer Security has over 15 years experience working within the security industry. Due to the increased government regulations, we have witnessed first-hand how it has evolved into a professional industry where safe and secure environments are being created for people to work and socialise in.</p>
						<p>We have made a vow to hire the most dedicated security professionals in the industry which include ex armed forces, police, paramedics and experienced security operatives. We apply the best security solutions available in order to deliver the highest value to our customers.</p>
						<p>As a reflection of our modern day approach, our standards and services have expanded into a wider range of fields where other companies have failed. We now operate throughout the whole of the UK with a strong business and management team in place to ensure guaranteed service, loyalty and confidentiality to all of our clients.</p>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6 col-6">
								<img src="/img/home1.png" class="img-fluid" />
							</div>
							<div class="col-md-6 col-6">
								<img src="/img/home2.png" class="img-fluid" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="new-services">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-6">
						<div class="service">
							<span class="fa fa-fw fa-building"></span>
							<h3>Construction</h3>
							<h2>Industry</h2>
							<hr />
							<p>Bauer Security provide a 5-star service to the construction industry including Manned Guards, Dog Handlers, Traffic Marshalls & CCTV – All Bauer Guards & Lone Workers are monitored regularly from our welfare centre.</p>
							<a href="/services/construction" class="btn btn-primary">Learn more</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-6">
						<div class="service">
							<span class="fa fa-fw fa-beer" style="padding-left: 15px;"></span>
							<h3>Entertainment</h3>
							<h2>Industry</h2>
							<hr />
							<p>Bauer Security Supply the Entertainment Industry with SIA licensed Security Personnel. We provide Nightclubs, Wine Bars, Public Houses, Hotels & Licensed Premises with experienced & professional Door Supervisors</p>
							<a href="/services/entertainment" class="btn btn-primary">Learn more</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-6">
						<div class="service">
							<span class="fa fa-fw fa-graduation-cap"></span>
							<h3>Education</h3>
							<h2>Industry</h2>
							<hr />
							<p>Bauer Security provide specially trained & qualified Security Guards to Colleges, Secondary Schools and Establishments of further education that require a more sophisticated approach towards Security.</p>
							<a href="/services/education" class="btn btn-primary">Learn more</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-6">
						<div class="service">
							<span class="fa fa-fw fa-briefcase"></span>
							<h3>Corporate</h3>
							<h2>Industry</h2>
							<hr />
							<p>Bauer Security provide offices blocks, retail parks and industrial estates with vetted Security Officers with concierge and lock-up experience. Our Security Officers are provided with bespoke training to suit our client’s corporate identity and tone of voice.</p>
							<a href="/services/corporate" class="btn btn-primary">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="icons">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="fa fa-fw fa-lock"></span>
							<h2>24 hour response team</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="fa fa-fw fa-award"></span>
							<h2>Commited to customer service</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="fa fa-fw fa-user-tie"></span>
							<h2>Dedicated account management</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="fa fa-fw fa-globe-americas"></span>
							<h2>Nationwide security coverage</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="fa fa-fw fa-leaf"></span>
							<h2>Carbon Neutral Security Company</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="fas fa-fw fa-shield-alt"></span>
							<h2>Over 8 Years Of Excellence</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="far fa-fw fa-id-card"></span>
							<h2>SIA Licensed &amp; CRB Checked Staff</h2>
						</div>
					</div>
					<div class="col-md-3 col-6">
						<div class="item">
							<span class="far fa-fw fa-envelope-open"></span>
							<h2>Free consultation &amp; quotations</h2>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="quote">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<img src="/img/liam.png" class="img-fluid" />
						<h2>Liam Gillett</h2>
						<p>Managing Director</p>
					</div>
					<div class="col-md-10">
						<div class="inner">
							<p class="the-quote">Since forming Bauer Security Ltd, I have devoted myself to providing a service that the security industry needed. I ensure that we provide a modern day, tailor made service to cater for all of our client’s needs. I set my standards extremely high, which filters down to our representatives, who are all well trained professionals in their field.</p>
							<p class="the-quote">I will continue to strive for excellence and take satisfaction in providing all of our clients with a service and relationship that sets us apart from the rest. I appreciate that representatives of Bauer Security Ltd are usually the first and last impressions on what your customers have on your business. Which is why I’m just as passionate about your business, as I am mine!</p>
							
							<div class="team">
								<div class="row">
									<div class="col-md-3">
										<a href="/meet-the-team" class="btn btn-default">Meet the team</a>
									</div>
									<div class="col-md-3">
										<a href="/join-the-team" class="btn btn-primary">Join the team</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			// Hero slider
			setInterval(function() {
				var active = $('.slide.active'),
					next = active.next('.slide');

				if(next.length == 0) {
					next = $('.slide:first');
				}

				// Fade out active
				active.animate({opacity: 0}, {queue: false, duration: 1000, complete: function() {
					$(this).removeClass('active');
				}});

				// Fade in next
				next.animate({opacity: 1}, {queue: false, duration: 1000, complete: function() {
					$(this).addClass('active');
				}});
			}, 2000);
		});
	</script>
@endsection