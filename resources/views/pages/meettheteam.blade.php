@extends('layouts.base')

@section('content')
	@include('common.header')
	<div class="page page-meet-the-team">
		<div class="page-title">
			<div class="container">
				<h2>Meet the Team</h2>
				<p>Meet the people behind the scenes</p>
			</div>
		</div>

		<section class="the-team">
			<div class="container">
				<div class="row">
					<div class="col-md-4 offset-md-4" style="margin-bottom: 70px;">
						<div class="team-member">
							<img src="/img/team/liam.jpg" class="img-fluid" />
							<h2>Liam Gillett</h2>
							<h3>Managing Director</h3>
							<p>Raising the bar within the Industry, building robust business relationships and ensuring client confidentiality & requirements are met and exceeded</p>
							<p><a href="mailto:liam@bauersecurity.co.uk">liam@bauersecurity.co.uk</a></p>
							<!-- <ul class="social">
								<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
								<li><a href="#"><span class="fab fa-twitter"></span></a></li>
								<li><a href="#"><span class="fab fa-linkedin"></span></a></li>
							</ul> -->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="team-member">
							<img src="/img/team/lawrence.jpg" class="img-fluid" />
							<h2>Lawrence Dajan</h2>
							<h3>Site Support Manager</h3>
							<p>Managing and supporting guarding contracts, adding value to service and injecting intelligence to immediate solutions</p>
							<p><a href="mailto:ops@bauersecurity.co.uk">ops@bauersecurity.co.uk</a></p>
							<!-- <ul class="social">
								<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
								<li><a href="#"><span class="fab fa-twitter"></span></a></li>
								<li><a href="#"><span class="fab fa-linkedin"></span></a></li>
							</ul> -->
						</div>
					</div>
					<div class="col-md-3">
						<div class="team-member">
							<img src="/img/team/catriona.jpg" class="img-fluid" />
							<h2>Catriona Smyth</h2>
							<h3>Office Manager</h3>
							<p>Overseeing accounts, managing the core of the team and ensuring a smooth, stress free relationship between businesses</p>
							<p><a href="mailto:admin@bauersecurity.co.uk">admin@bauersecurity.co.uk</a></p>
							<!-- <ul class="social">
								<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
								<li><a href="#"><span class="fab fa-twitter"></span></a></li>
								<li><a href="#"><span class="fab fa-linkedin"></span></a></li>
							</ul> -->
						</div>
					</div>
					<div class="col-md-3">
						<div class="team-member">
							<img src="/img/team/jason.jpg" class="img-fluid" />
							<h2>Jason Velez</h2>
							<h3>Response Manager</h3>
							<p>Responding to Emergency situations, implementing military experience, liaising with relevant officials and adding substance to service</p>
							<p><a href="mailto:response@bauersecurity.co.uk">response@bauersecurity.co.uk</a></p>
							<!-- <ul class="social">
								<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
								<li><a href="#"><span class="fab fa-twitter"></span></a></li>
								<li><a href="#"><span class="fab fa-linkedin"></span></a></li>
							</ul> -->
						</div>
					</div>
					<div class="col-md-3">
						<div class="team-member">
							<img src="/img/team/alastaire.jpg" class="img-fluid" />
							<h2>Alastaire Salvage</h2>
							<h3>DS Support Manager</h3>
							<p>Supporting Licensed premises with a confident approach and adding unrivalled experience which exceeds more than a decade of Event Management and Front-Line logistics</p>
							<p><a href="mailto:support@bauersecurity.co.uk">support@bauersecurity.co.uk</a></p>
							<!-- <ul class="social">
								<li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
								<li><a href="#"><span class="fab fa-twitter"></span></a></li>
								<li><a href="#"><span class="fab fa-linkedin"></span></a></li>
							</ul> -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection