@extends('layouts.base')

@section('content')
	<div class="page page-cctv">
		<div class="hero hero-cctv">
			<div class="slider">
				<div class="slide active slide-1"></div>
			</div>

			<div class="container">
				<ul class="social animated slideInDown">
					@include('common.sociallinks')
				</ul>

				<ul class="details animated slideInDown">
					<li><a href="mailto:ops@bauersecurity.co.uk" style="color: #fff;">ops@bauersecurity.co.uk</a></li>
					<li><a href="#" style="color: #fff;">0845 241 0074</a></li>
				</ul>

				<div class="logo animated slideInDown">
					<a href="/">
						<img src="/img/logo-half.png" class="img-fluid" />
					</a>
				</div>

				<ul class="main-nav animated bounceIn">
					@include('common.menuitems')
				</ul>

				<div class="caption animated flipInX">
					<h2>World class CCTV Systems</h2>
					<h3>To keep you feeling safe at home</h3>
					<hr />
					<p>Bauer Security - Established 2009</p>
					<span class="fa fa-arrow-circle-down"></span>
				</div>
			</div>
		</div>

		<section class="story">
			<div class="container">
				<div class="row">
					<div class="col-md-8 offset-md-2 text-center">
						<h3>CCTV Systems</h3>
						<hr />
						<p>CCTV Systems are a proven and effective means of break in prevention, not only do they act as both a prominent visual deterrent, they also provide a clear record of incident to the police and to the authorities. We install high definition CCTV systems from the worlds leading manufacturer – Hikvision. The systems that we install can be monitored remotely, which is why they are commonly installed alongside our Intruder alarm systems to provide maximum protection.</p>
						<p>Health & Safety continues to become a bigger part of the day to day running of almost all companies in every industry. More and more companies are using CCTV as a means of staff monitoring, providing a tangible audit trail should there be a HSE reportable incident falling under RIDDOR.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="service">
							<img src="http://www.ccl-uk.co.uk/wp-content/uploads/2017/01/IP-CCTV-Systems.jpg" class="img-fluid" />
							<h4>IP Based CCTV</h4>
							<p>Almost all new CCTV installations are IP systems. Stable and secure – IP systems typically utilise a CAT5 data cable to send images from your cameras to a central DVR (Digital Video Recorder) they are High Definition and provide super clear images.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service">
							<img src="http://www.ccl-uk.co.uk/wp-content/uploads/2017/01/ds-2ce16d5t-vfit3_vf_bullet.png" class="img-fluid" />
							<h4>Turbo HD CCTV</h4>
							<p>Have an existing analogue system but want to upgrade? Look no further! Turbo HD allows us to retrofit a new system to your existing coaxial cable, whilst upgrading your cameras and DVR to a high definition setup – saving time and cost.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service">
							<img src="http://www.ccl-uk.co.uk/wp-content/uploads/2017/01/Analogue-Camera-Systems.jpg" class="img-fluid" />
							<h4>Analogue CCTV</h4>
							<p>For customers with analogue systems, we are still able to add additional cameras and extras to your existing system. For up to date coverage we do however recommend our customers consider looking at our Turbo HD retrofit system.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="icons">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="icon">
							<span class="fa fa-key"></span>
							<h2>Door Entry Systems</h2>
						</div>
					</div>
					<div class="col-md-3">
						<div class="icon">
							<span class="fa fa-headphones"></span>
							<h2>Audio Visual</h2>
						</div>
					</div>
					<div class="col-md-3">
						<div class="icon">
							<span class="fa fa-lock"></span>
							<h2>Access Control</h2>
						</div>
					</div>
					<div class="col-md-3">
						<div class="icon">
							<span class="fa fa-sitemap"></span>
							<h2>Structured Data Cabling</h2>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('scripts')
	<!-- <script type="text/javascript">
		$(document).ready(function() {
			// Hero slider
			setInterval(function() {
				var active = $('.slide.active'),
					next = active.next('.slide');

				if(next.length == 0) {
					next = $('.slide:first');
				}

				// Fade out active
				active.animate({opacity: 0}, {queue: false, duration: 1000, complete: function() {
					$(this).removeClass('active');
				}});

				// Fade in next
				next.animate({opacity: 1}, {queue: false, duration: 1000, complete: function() {
					$(this).addClass('active');
				}});
			}, 2000);
		});
	</script> -->
@endsection