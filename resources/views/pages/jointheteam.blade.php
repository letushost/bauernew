@extends('layouts.base')

@section('content')
	@include('common.header')
	<div class="page page-join-the-team">
		<div class="page-title">
			<div class="container">
				<h2>Join the Team</h2>
				<p>Think you've got what it takes?</p>
			</div>
		</div>

		<section class="team-form">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="intro">
							<h2>Apply to work with us</h2>
							<p>Our Guards are the backbone of the business. As part of the Bauer Security team, you will be valued, treated with respect and be given opportunities for professional progression. We offer you a membership, not just a job, and more than a third of our team have been with us for over five years.</p>
							<p>If you are interested in joining the Bauer Security team, simply fill out the form on this page and one of our recruitment managers will be in touch within two working days.</p>
							<p>Our application process includes a written English language test and we will require original forms of identification and proof of address, so please have them ready before you apply.</p>
						</div>
					</div>
					<div class="col-md-8">
						<form>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Full name</label>
										<input type="text" class="form-control" placeholder="Full name" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Email address</label>
										<input type="text" class="form-control" placeholder="Email address" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Phone number</label>
										<input type="text" class="form-control" placeholder="Phone number" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Address line 1</label>
										<input type="text" class="form-control" placeholder="Address line 1" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>City</label>
										<input type="text" class="form-control" placeholder="City" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Postcode</label>
										<input type="text" class="form-control" placeholder="Postcode" />
									</div>
								</div>
								<div class="col-md-6">
									<a href="#" class="attach-cv">Attach CV</a>
									<input type="file" name="cv" style="display: none;" />
								</div>
								<div class="col-md-6">
									<select class="form-control">
										@foreach($services as $service)
											<option value="{{ $service->id }}">{{ $service->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Personal Statement</label>
										<textarea class="form-control" placeholder="Personal statement"></textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Additional information</label>
										<textarea class="form-control" placeholder="Additional information"></textarea>
									</div>
								</div>
								<div class="col-md-12" style="margin-top: 10px;">
									<input type="submit" class="btn btn-primary" value="Send message" />
							    	<p class="drop-info">We aim to reply to all messages within 24 hours</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.attach-cv').on('click', function(e) {
				e.preventDefault();

				$('input[type="file"]').trigger('click');
			});

			$('input[type="file"]').on('change', function(e) {
				var filename = $(this).val().split('\\').pop();
				
				$('.attach-cv').html('Attach CV <span class="float-right">' + filename + '</span>');
			});
		});
	</script>
@endsection