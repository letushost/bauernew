@extends('layouts.base')

@section('content')
	@include('common.header')
	<div class="page page-contact">
		<div class="page-title">
			<div class="container">
				<h2>Contact us</h2>
				<p>Get in touch with us</p>
			</div>
		</div>

		<div class="container">
			<div class="contact-methods">
				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12 col-sm-6">
								<h3><span class="far fa-map"></span> Address</h3>
								<p>Unit 10, Bulrushes Farm Business Park<br />
								Coombe Hill Road<br />
								East Grinstead<br />
								RH19 4LZ</p>
							</div>

							<div class="col-md-12 col-sm-6">
								<h3><span class="fas fa-mobile-alt"></span> Phone</h3>
								<p>Call: 0845 241 0074<br />
								Fax: 01342 311 101</p>
							</div>

							<div class="col-md-12">
								<h3><span class="far fa-envelope"></span> Email</h3>
								<p><a href="mailto:ops@bauersecurity.co.uk">ops@bauersecurity.co.uk</a><br />
								<a href="mailto:admin@bauersecurity.co.uk">admin@bauersecurity.co.uk</a></p>
								<a href="mailto:support@bauersecurity.co.uk">support@bauersecurity.co.uk</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-8">
				    	<form>
				    		<div class="row">
				    			<div class="col-md-6">
						    		<div class="form-group">
						    			<input type="text" class="form-control" placeholder="Name" />
						    		</div>
						    	</div>
						    	<div class="col-md-6">
						    		<div class="form-group">
						    			<input type="text" class="form-control" placeholder="Email address" />
						    		</div>
						    	</div>
						    </div>
				    		<div class="row">
				    			<div class="col-md-6">
						    		<div class="form-group">
						    			<input type="text" class="form-control" placeholder="Phone number" />
						    		</div>
						    	</div>
						    	<div class="col-md-6">
						    		<div class="form-group">
						    			<input type="text" class="form-control" placeholder="Subject" />
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
						    	<div class="col-md-12">
						    		<div class="form-group">
						    			<textarea class="form-control" rows="3" placeholder="Enter your message"></textarea>
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
							    <div class="col-md-12" style="margin-top: 15px">
							    	<input type="submit" class="btn btn-primary" value="Send message" />
							    	<p class="drop-info">We aim to reply to all messages within 24 hours</p>
							    </div>
							</div>
				    	</form>
					</div>
				</div>
			</div>
		</div>

		<div class="map">
			<div class="mapouter">
				<div class="gmap_canvas">
					<div id="map" style="width: 100%; height: 100%;"></div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoZNRLMrrdV1Z55oQiOSQhagUqhEOAnEg"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                var mapOptions = {
                    zoom: 15,
                    center: new google.maps.LatLng(51.1152713, 0.0174207),
                    styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
                };
                var mapElement = document.getElementById('map');
                var map = new google.maps.Map(mapElement, mapOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(51.1152713, 0.0174207),
                    map: map,
                    title: 'Snazzy!'
                });
            }
		});
	</script>
@endsection