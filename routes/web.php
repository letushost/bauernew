<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('home');
Route::get('/services/{type?}', 'PageController@services')->name('services');
Route::get('/join-the-team', 'PageController@jointheteam')->name('join-the-team');
Route::get('/meet-the-team', 'PageController@meettheteam')->name('meet-the-team');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/cctv', 'PageController@cctv')->name('cctv');