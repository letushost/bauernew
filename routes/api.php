<?php

// Dirty X-Access hack
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
header('Access-Control-Allow-Methods: GET, POST, DELETE, PUT');

// Auth protected routes
Route::group(['middleware' => 'auth.host'], function () {
	Route::group(['prefix' => 'jobs'], function() {
		Route::get('/', 'JobController@all');
		Route::get('/my', 'JobController@my');
		Route::get('/{job}', 'JobController@single');
		Route::put('/{job}/checkin', 'JobController@checkin');
	});
});

// Unprotected routes
Route::group(['prefix' => 'auth'], function() {
	Route::post('/login', 'AuthController@standard');
});

Route::group(['prefix' => 'test'], function() {
	Route::get('/', function() {
		return ['test'];
	});
});